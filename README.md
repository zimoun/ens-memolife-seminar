# Toward practical transparent, verifiable and long-term reproducible research
## using Guix

Talk for [bioinfo seminar](https://www.ibens.ens.fr/spip.php?article172) part
of [Labex Memolife](https://www.memolife.biologie.ens.fr/).

# Build

```
guix time-machine -C channels.scm \
     -- shell -m manifest.scm     \
     -- rubber --pdf stournier-Guix-20221201.tex
```

+ Git: https://gitlab.com/zimoun/ens-memolife-seminar
+ SWH: https://archive.softwareheritage.org/swh:1:dir:1b08e6efba8fdee46e32c75b57ce7f95c43da558
